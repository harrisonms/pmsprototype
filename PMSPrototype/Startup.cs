﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PMSPrototype.Startup))]
namespace PMSPrototype
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
